import sys
from typing import Optional
import os


class DesktopEntry:
    def __init__(self,
                 name: str,
                 icon: Optional[str] = None,
                 terminal: bool = True,
                 encoding: str = "UTF-8",
                 _type: str = "Application",
                 _exec: Optional[str] = None,
                 **kwargs):
        self.name = name
        self.icon = icon
        self.exec = _exec or sys.executable + " " + "/".join(sys.executable.split("/")[:-3]) + "/main.py"
        self.terminal = str(terminal).lower()
        self.encoding = encoding
        self.type = _type
        self._more = {x: kwargs[x] for x in kwargs} or dict()

    def get_dict(self) -> dict:
        return {
            **self._more,
            **{
                x: self.__dict__[x] for x in dir(self)
                if not x.startswith("_")
                and x in self.__dict__
                and not callable(self.__dict__[x])
            },
        }

    def __str__(self) -> str:
        d = self.get_dict()
        return "\n".join(
            ["[Desktop Entry]"] +
            [f"{x.title()}={d[x] or str()}" for x in d if d[x]]
        )

    def write(self, path: str, mode: str = "w"):
        with open(path, mode) as file:
            file.write(str(self))
            file.close()


def automatic(path: str = "~/.local/share/applications/", **kwargs):
    path = os.path.expanduser(path)
    if os.path.isdir(path):
        name = sys.executable.split("/")[-4:-3][0] + ".desktop"
        path = path + name
    elif os.path.isfile(path):
        name = path.split("/")[-1]
    else:
        raise ValueError(f"Path is incompatible: {path}")
    de = DesktopEntry(
        name=name.rstrip(".desktop").replace("_", " ").title(),
        icon=None,
        terminal=True,
        **kwargs
    )
    de.write(path=path)
